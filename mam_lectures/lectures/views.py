from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView

from lectures.forms import TaskForm
from lectures.models import Task


class TaskListView(TemplateView):
    """Template view that displays list of Tasks and some other info."""
    template_name = 'lectures/index.html'

    def get_context_data(self):
        context = {}
        context['user'] = {
            'name': 'honza',
            'login': 'stinovlas',
            'age': 30,
        }
        context['list'] = list(range(10))
        context['tasks'] = Task.objects.all()
        return context


class CreateTaskView(CreateView):
    """
    This view uses generic CreateView.

    We can find the corresponding template in `lectures/task_form.html`
    """
    model = Task
    fields = ['title', 'score', 'is_public']
    success_url = reverse_lazy('lectures:index')


class UpdateTaskView(TemplateView):
    """
    A little more complicated update view.

    We could also use generic UpdateView class, this is for illustrating purposes.
    """
    template_name = 'lectures/task_update.html'

    def get_context_data(self, task_id):
        task = get_object_or_404(Task, pk=task_id)
        form = TaskForm(instance=task)
        return {'form': form}

    def post(self, request, task_id):
        task = get_object_or_404(Task, pk=task_id)
        form = TaskForm(request.POST)
        if form.is_valid():
            task.title = form.cleaned_data['title']
            task.score = form.cleaned_data['score']
            task.is_public = form.cleaned_data['is_public']
            task.author = form.cleaned_data['author']
            task.save()
            return HttpResponseRedirect(reverse('lectures:index'))
        else:
            return self.render_to_response({'form': form})


def hello_world(request, times=1):
    """Simple function based view."""
    return HttpResponse('Hello, world ({}x)!'.format(times))
