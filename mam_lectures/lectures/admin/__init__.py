from django import forms
from django.contrib import admin

from lectures.models import Organizer, Student, Task

from .forms import TaskAdminForm


class TaskAdmin(admin.ModelAdmin):
    form = TaskAdminForm


admin.site.register(Task, TaskAdmin)
admin.site.register(Organizer)
admin.site.register(Student)
