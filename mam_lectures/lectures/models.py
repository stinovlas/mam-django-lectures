from django.db import models

from lectures.constants import EyeColor


class Organizer(models.Model):
    name = models.TextField("Name", null=False, blank=False)

    def __str__(self):
        return self.name

class Task(models.Model):
    title = models.TextField("Title")
    score = models.IntegerField("Maximum score")
    is_public = models.BooleanField("Is public", default=False)
    deadline = models.DateTimeField("Deadline", null=True, blank=True)
    author = models.ForeignKey(Organizer, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return '{} ({})'.format(self.title, self.score)


EYE_COLOR_CHOICES = (
    (EyeColor.BLUE, "Blue"),
    (EyeColor.GREEN, "Green"),
    (EyeColor.BROWN, "Brown"),
)

class Student(models.Model):
    eye_color = models.CharField("Eye color", max_length=10, null=False, default=EyeColor.BROWN, choices=EYE_COLOR_CHOICES)
    solved_tasks = models.ManyToManyField(Task, related_name='students')
