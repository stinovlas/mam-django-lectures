from enum import Enum, unique


@unique
class EyeColor(str, Enum):
    BLUE = 'blue'
    GREEN = 'green'
    BROWN = 'brown'
