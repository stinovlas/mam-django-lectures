from django.urls import path

from lectures.views import (CreateTaskView, TaskListView, UpdateTaskView,
                            hello_world)

app_name = 'lectures'

urlpatterns = [
    path('hello-world/', hello_world, name='hello'),
    path('hello-world/<int:times>/', hello_world, name='hello'),
    path('', TaskListView.as_view(), name='index'),
    path('create-task/', CreateTaskView.as_view(), name='create_task'),
    path('update-task/<int:task_id>/', UpdateTaskView.as_view(), name='update_task'),
]
