from django import forms
from django.core.validators import validate_email

from lectures.models import Task


class ExampleForm(forms.Form):
    """This form is not used anywhere, it's just a demonstration of form with custom validation."""
    title = forms.CharField(validators=[validate_email])
    score = forms.IntegerField(required=False)

    def clean_title(self):
        return self.cleaned_data['title'].lower()

    def clean_score(self):
        if self.cleaned_data['score'] < 0:
            raise forms.ValidationError('Score must be non-negative', code='negative')
        return self.cleaned_data['score']


class TaskForm(forms.ModelForm):
    """This is a good example of ModelForm."""
    class Meta:
        model = Task
        fields = ['title', 'score', 'is_public', 'author']
        widgets = {
            'title': forms.TextInput(),
        }
