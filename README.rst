==============
 mam_lectures
==============

Vzorový projekt ke kurzu Djanga pro organizátory `M&M <http://mam.matfyz.cz>`_.


Návod k instalaci
=================

Doporučuji použít program ``pipenv``. Pokud jej ještě nemáte, můžete ho nainstalovat z balíčků vaší distribuce (pokud
existují), nebo příkazem:

.. code-block:: bash

   pip3 install --user pipenv

Pokud jste tak ještě neučinili, vytvořte nový *virtualenv*:

.. code-block:: bash

   pipenv --three

Poté nainstalujte závislosti našeho projektu pomocí:

.. code-block:: bash

   pipenv install -r requirements.txt
